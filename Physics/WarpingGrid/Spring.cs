﻿namespace Physics.WarpingGrid
{
    using Microsoft.Xna.Framework;

    public class Spring
    {
        const float DEFAULT_LENGTH_FACTOR = 0.9f;

        protected PointMass start;
        protected PointMass end;

        protected float targetLength;
        protected float stiffness;
        protected float damping;

        public Spring(PointMass start, PointMass end, float stiffness, float damping)
        {
            this.start = start;
            this.end = end;

            this.stiffness = stiffness;
            this.damping = damping;

            this.targetLength = Vector3.Distance(start.Position, end.Position) * DEFAULT_LENGTH_FACTOR;
        }

        public void Update()
        {
            var x = start.Position - end.Position;

            float length = x.Length();

            if (length <= this.targetLength)
            {
                return;
            }

            x = (x / length) * (length - this.targetLength);

            var deltaVelocity = end.Velocity - start.Velocity;
            var force = this.stiffness * x - deltaVelocity * this.damping;

            start.ApplyForce(-force);
            end.ApplyForce(force);
        }
    }
}
