﻿namespace Physics.WarpingGrid
{
    using Physics.Library;
    using Microsoft.Xna.Framework;

    public class PointMass : RigidBody3D
    {
        const float DRAG_COEFFICIENT = 0.98f;

        public PointMass(Vector3 position, float inverseMass)
            : base(position, inverseMass, PointMass.DRAG_COEFFICIENT)
        { }

        public override void IncreaseDampingFactor(float factor)
        {
            this.drag *= factor;
        }

        public override void Update()
        {
            base.Update();

            this.drag = DRAG_COEFFICIENT;
        }
    }
}
