﻿namespace Physics.WarpingGrid
{
    using Physics.Library;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using System.Collections.Generic;
    using System;
    
    public class Grid
    {
        const float SPRING_STIFFNESS = 0.06f;
        const float SPRING_DAMPING = 0.06f;

        protected static GraphicsDevice graphicsDevice;

        protected Spring[] springs;
        protected PointMass[,] intersectionPoints;

        protected Texture2D springTexture;


        public Grid(Rectangle size, Vector2 spacing, Texture2D springTexture, Color color)
        {
            this.springTexture = springTexture;

            var springList = new List<Spring>();

            int numColumns = (int)(size.Width / spacing.X) + 1;
            int numRows = (int)(size.Height / spacing.Y) + 1;
            intersectionPoints = new PointMass[numColumns, numRows];

            PointMass[,] fixedPoints = new PointMass[numColumns, numRows];

            int column = 0;
            int row = 0;

            for (float y = size.Top; y <= size.Bottom; y += spacing.Y)
            {
                for (float x = size.Left; x <= size.Right; x += spacing.X)
                {
                    intersectionPoints[column, row] = new PointMass(new Vector3(x, y, 0), 1);
                    fixedPoints[column, row] = new PointMass(new Vector3(x, y, 0), 0);
                    column++;
                }
                row++;
                column = 0;
            }

            // link the point masses with springs
            for (int y = 0; y < numRows; y++)
                for (int x = 0; x < numColumns; x++)
                {
                    if (x == 0 || y == 0 || x == numColumns - 1 || y == numRows - 1)    // anchor the border of the grid 
                        springList.Add(new Spring(fixedPoints[x, y], intersectionPoints[x, y], 0.1f, 0.1f));
                    else if (x % 3 == 0 && y % 3 == 0)                                  // loosely anchor 1/9th of the point masses 
                        springList.Add(new Spring(fixedPoints[x, y], intersectionPoints[x, y], 0.002f, 0.02f));

                    if (x > 0)
                        springList.Add(new Spring(intersectionPoints[x - 1, y], intersectionPoints[x, y], SPRING_STIFFNESS, SPRING_DAMPING));
                    if (y > 0)
                        springList.Add(new Spring(intersectionPoints[x, y - 1], intersectionPoints[x, y], SPRING_STIFFNESS, SPRING_DAMPING));
                }

            springs = springList.ToArray();
        }

        public void Update()
        {
            foreach (Spring spring in this.springs)
            {
                spring.Update();
            }

            foreach (PointMass point in this.intersectionPoints)
            {
                point.Update();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int width = intersectionPoints.GetLength(0);
            int height = intersectionPoints.GetLength(1);
            Color color = Color.DarkBlue;   // dark blue

            for (int y = 1; y < height; y++)
            {
                for (int x = 1; x < width; x++)
                {
                    Vector2 left = new Vector2(), up = new Vector2(); Vector2 p = ToVec2(intersectionPoints[x, y].Position); if (x > 1)
                    {
                        left = ToVec2(intersectionPoints[x - 1, y].Position);
                        float thickness = y % 3 == 1 ? 3f : 1f;

                        // use Catmull-Rom interpolation to help smooth bends in the grid
                        int clampedX = Math.Min(x + 1, width - 1);
                        Vector2 mid = Vector2.CatmullRom(ToVec2(intersectionPoints[x - 2, y].Position), left, p, ToVec2(intersectionPoints[clampedX, y].Position), 0.5f);

                        // If the grid is very straight here, draw a single straight line. Otherwise, draw lines to our
                        // new interpolated midpoint
                        if (Vector2.DistanceSquared(mid, (left + p) / 2) > 1)
                        {
                            spriteBatch.DrawLine(this.springTexture, left, mid, color, thickness);
                            spriteBatch.DrawLine(this.springTexture, mid, p, color, thickness);
                        }
                        else
                            spriteBatch.DrawLine(this.springTexture, left, p, color, thickness);
                    }

                    if (y > 1)
                    {
                        up = ToVec2(intersectionPoints[x, y - 1].Position);
                        float thickness = x % 3 == 1 ? 3f : 1f;
                        spriteBatch.DrawLine(this.springTexture, up, p, color, thickness);
                    }

                    if (x > 1 && y > 1)
                    {
                        Vector2 upLeft = ToVec2(intersectionPoints[x - 1, y - 1].Position);
                        spriteBatch.DrawLine(this.springTexture, 0.5f * (upLeft + up), 0.5f * (left + p), color, 1f);   // vertical line
                        spriteBatch.DrawLine(this.springTexture, 0.5f * (upLeft + left), 0.5f * (up + p), color, 1f);   // horizontal line
                    }
                }
            }
        }

        public void ApplyDirectedForce(Vector3 force, Vector3 position, float radius)
        {
            foreach (var mass in intersectionPoints)
                if (Vector3.DistanceSquared(position, mass.Position) < radius * radius)
                    mass.ApplyForce(10 * force / (10 + Vector3.Distance(position, mass.Position)));
        }

        public void ApplyImplosiveForce(float force, Vector3 position, float radius)
        {
            foreach (var mass in intersectionPoints)
            {
                float dist2 = Vector3.DistanceSquared(position, mass.Position);
                if (dist2 < radius * radius)
                {
                    mass.ApplyForce(10 * force * (position - mass.Position) / (100 + dist2));
                    mass.IncreaseDampingFactor(0.6f);
                }
            }
        }

        public void ApplyExplosiveForce(float force, Vector3 position, float radius)
        {
            foreach (var mass in intersectionPoints)
            {
                float dist2 = Vector3.DistanceSquared(position, mass.Position);
                if (dist2 < radius * radius)
                {
                    mass.ApplyForce(100 * force * (mass.Position - position) / (10000 + dist2));
                    mass.IncreaseDampingFactor(0.6f);
                }
            }
        }

        public Vector2 ToVec2(Vector3 v)
        {
            // do a perspective projection
            float factor = (v.Z + 2000) / 2000;
            return (new Vector2(v.X, v.Y) - new Vector2(Game1.instance.GraphicsDevice.Viewport.Width, Game1.instance.GraphicsDevice.Viewport.Height) / 2f) * factor + new Vector2(Game1.instance.GraphicsDevice.Viewport.Width, Game1.instance.GraphicsDevice.Viewport.Height) / 2;
        }
    }
}
