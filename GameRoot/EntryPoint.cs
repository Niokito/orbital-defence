﻿namespace OrbitalDefence
{
    using System;

    public static class EntryPoint
    {
        [STAThread]
        private static void Main()
        {
            using (var game = new GameRoot())
            {
                game.Run();
            }
        }
    }
}
