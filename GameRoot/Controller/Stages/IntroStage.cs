﻿namespace OrbitalDefence.Controller.Stages
{
    using OrbitalDefence.Model;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;
    using Physics.WarpingGrid;
    using System;
    using OrbitalDefence.Model.Enumerations;

    public class IntroStage : Stage
    {
        public IntroStage(Song backgroundMusic, Camera2D camera, GameStage type)
            : base(backgroundMusic, camera, type)
        {
        }

        public override void Initialize()
        {
            this.BackgroundMusic = Assets.Audible.MainMenuMusic;

            base.Initialize();
        }

        public override void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            if (keyboardState.IsKeyDown(Keys.Space))
            {
                this.isExpired = true;
            }
        }

        public override void Draw(SpriteBatch spriteBatch, bool bloomPass)
        {
            spriteBatch.DrawString(Assets.Visual.FontLarge, "orbital defence", new Vector2(1920 / 3, 150), Color.White);

        }
    }
}
