﻿namespace OrbitalDefence.Controller
{

    using System;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;
    using Microsoft.Xna.Framework.Graphics;
    using OrbitalDefence.Model.Enumerations;

    public abstract class Stage
    {
        public Stage(Song backgroundMusic, Camera2D camera, GameStage type)
        {
            this.BackgroundMusic = backgroundMusic;
            this.Camera = camera;
            this.Type = type;
        }

        public Song BackgroundMusic { get; set; }

        public Camera2D Camera { get; set; }

        public bool isExpired { get; protected set; }

        public bool ExitGame { get; set; }

        public GameStage Type { get; set; }

        public virtual void Initialize()
        {
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(this.BackgroundMusic);
        }

        public abstract void Update(GameTime gameTime, KeyboardState keyboardState);

        public abstract void Draw(SpriteBatch spriteBatch, bool bloomPass);


    }
}
