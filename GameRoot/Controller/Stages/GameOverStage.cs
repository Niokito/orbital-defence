﻿namespace OrbitalDefence.Controller.Stages
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;
    using OrbitalDefence.Model;
    using OrbitalDefence.Model.Enumerations;

    public class GameOverStage : Stage
    {
        public GameOverStage(Song backgroundMusic, Camera2D camera, GameStage type)
            : base(backgroundMusic, camera, type)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            this.Camera.Update(new Vector2(1920/2, 1080/2));
            if (keyboardState.IsKeyDown(Keys.Space) || keyboardState.IsKeyDown(Keys.Escape) || keyboardState.IsKeyDown(Keys.Enter))
            {
                this.isExpired = true;
            }
        }

        public override void Draw(SpriteBatch spriteBatch, bool bloomPass)
        {
            spriteBatch.DrawString(Assets.Visual.FontLarge, "game over", new Vector2(1920 / 2, 1080 / 2), Color.White);
        }
    }
}
