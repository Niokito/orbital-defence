﻿namespace OrbitalDefence.Controller.Stages
{
    using OrbitalDefence.Model;
    using OrbitalDefence.Model.Enumerations;
    using Physics.WarpingGrid;
    using Physics.ParticleEffects;
    using Physics.Library;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Microsoft.Xna.Framework.Media;
    using Microsoft.Xna.Framework.Audio;
    using OrbitalDefence.Model.Units;

    public class GameplayStage : Stage
    {
        const int PLAYER_SPAWN_DELAY = 1500;
        const int ENEMIES_SPAWN_DELAY = 3000;

        static double elaspedSeconds;
        static double elaspedMilliseconds;

        /* Gameplay configuration */

        // Number of enemies in a wave.
        // Starts from 2 and incremented for every wave with 1
        // Change incrementation to modify gameplay
        static int enemiesInWave = 2;

        // Number of seconds between waves.
        // Starts from 4 and incremented for every wave wtih 1
        // Change incrementation to modify gameplay
        static int waveRespawnTime = 4;

        // Counts how many enemies from a initiated wave are respawned
        // When 0 all enemies from a wave are respawnd
        // Set to the count of the wave and decrement as enemies are created
        static int enemiesToSpawnFromCurrentWave = 0;

        static double lastEnemySpawnedTime = 0;

        // Delay between enemies respawn in a wave
        static int enemyRespawnDelay = 100;

        // Records the total game seconds at which a wave was respawned
        static double lastWaveRespawnGameElaspedTime = 0;

        internal readonly GameTime StageTime = new GameTime();

        private Rectangle playfield;

        public GameplayStage(int screenWidth, int screenHeight, Camera2D camera, Song backgroundMusic, GameStage type)
            : base(backgroundMusic, camera, type)
        {   
            this.playfield = new Rectangle(0, 0, screenWidth * 2, screenHeight * 2);
        }

        private Grid backgroundGrid;

        private ParticleManager<ParticleState> particleManager;

        public override void Initialize()
        {
            this.AddStation();
            StageManagerComponent.AddPlayer();

            EntityController.Playfield = this.playfield;

            int maxGridPoints = 400;

            Vector2 gridSpacing = new Vector2((float)Math.Sqrt((playfield.Width / 2  * playfield.Height / 2) / maxGridPoints));

            this.particleManager = new ParticleManager<ParticleState>(1024 * 20, ParticleState.UpdateParticle);

            this.backgroundGrid = new Grid(this.playfield, gridSpacing, Assets.Visual.BackgroundGridTexture, Color.DarkBlue);

            this.Camera.View = new Viewport(this.playfield.X, this.playfield.Y, this.playfield.Width / 2, this.playfield.Height / 2);
            
            base.Initialize();
        }

        public override void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            this.CheckWaveState(gameTime);

            this.SpawnPlayerShip();

            EntityController.Update(gameTime);

            if (StageManagerComponent.Player.isAlive)
            {
                this.Camera.Update(StageManagerComponent.Player.ControlledUnit.Position2D);
            }

            this.HandleInput(keyboardState);

            this.particleManager.Update();
            this.backgroundGrid.Update();

            if (EntityController.PlayerAssets[1].IsExpired)
            {
                this.isExpired = true;
            }
        }

        private void SpawnPlayerShip()
        {
            if (!StageManagerComponent.Player.isAlive)
            {
                PlayerShip newShip = new PlayerShip(Assets.Visual.PlayerShipModel, new Vector3(this.playfield.Center.ToVector2(), 0), 1.5f, 0.98f);
                
                newShip.OnShoot += this.SpacecraftShotHandler;
                newShip.OnMove += this.PlayerMoveHandler;
                newShip.OnDamageTake += this.PlayerDamageTake;

                StageManagerComponent.Player.SetNewControlledUnit(newShip);
                EntityController.PlayerAssets.Add(newShip);
            }
        }

        public override void Draw(SpriteBatch spriteBatch, bool bloomPass)
        {
           
            foreach (Spacecraft ship in EntityController.PlayerAssets)
            {
                ship.Draw(spriteBatch);
            }

            foreach (EnemyShip ship in EntityController.EnemyShips)
            {
                ship.Draw(spriteBatch);
            }

            foreach (Projectile projectile in EntityController.Projectiles)
            {
                projectile.Draw(spriteBatch);
            }

            this.particleManager.Draw(spriteBatch);

            if (bloomPass)
            {
                this.backgroundGrid.Draw(spriteBatch, new Viewport(0, 0, this.playfield.Width / 2, this.playfield.Height / 2), StageManagerComponent.Player.ControlledUnit.Position2D);
            }
        }

        private void HandleInput(KeyboardState keyboardState)
        {
            foreach (Keys key in StageManagerComponent.Player.ControlScheme.Keys)
            {
                if (keyboardState.IsKeyDown(key))
                {
                    // Execute the delegated method for the command corresponding to the press key
                    // TODO: Multiple player support
                    // TODO: Make it human readable :) (Class)
                    StageManagerComponent.Player.ControlMap[StageManagerComponent.Player.ControlScheme[key]].Invoke();
                }

                if (key == Keys.F10)
                {
                    EntityController.KillAll();
                }
            }
        }

        private void AddStation()
        {
            SpaceStation station = new SpaceStation(Assets.Visual.SpaceStationLevel1, new Vector3(this.playfield.Center.ToVector2(), 0));
            EntityController.PlayerAssets.Add(station);
        }

        #region EnemyWaveLogic

        private void CheckWaveState(GameTime gameTime)
        {
            elaspedMilliseconds = gameTime.TotalGameTime.TotalMilliseconds;
            elaspedSeconds = gameTime.TotalGameTime.TotalSeconds;

            if (((elaspedSeconds - lastWaveRespawnGameElaspedTime) >= waveRespawnTime ||
                EntityController.EnemyShips.Count == 0) &&
                enemiesToSpawnFromCurrentWave == 0)
            {
                lastWaveRespawnGameElaspedTime = elaspedSeconds;
                SetSpawnWaveParameters();
            }

            UpdateCurrentWave();
        }

        private void UpdateCurrentWave()
        {
            if (enemiesToSpawnFromCurrentWave > 0 && elaspedMilliseconds - lastEnemySpawnedTime > enemyRespawnDelay && EntityController.EnemyShips.Count < 25)
            {
                lastEnemySpawnedTime = elaspedMilliseconds;
                SpawnRandomEnemy();
                enemiesToSpawnFromCurrentWave--;
            }
        }

        private void SetSpawnWaveParameters()
        {
            enemiesToSpawnFromCurrentWave = enemiesInWave;
            enemyRespawnDelay = 1000 / enemiesInWave;

            enemiesInWave++;
            waveRespawnTime++;
        }

        private void SpawnRandomEnemy()
        {
            Point randomPoint = this.GetEnemySpawnPosition();

            Array types = Enum.GetValues(typeof(EnemyType));
            EnemyType randomType = (EnemyType)types.GetValue(GameRoot.Random.Next(types.Length));

            EnemyShip newEnemy = EnemyShip.GetEnemyShip(randomType, new Vector3(randomPoint.X, randomPoint.Y, 0));

            newEnemy.OnDeath += EnemyDeathHandler;
            newEnemy.OnShoot += SpacecraftShotHandler;

            EntityController.EnemyShips.Add(newEnemy);
        }
        
        private Point GetEnemySpawnPosition()
        {
            int randomX = GameRoot.Random.Next(0, this.playfield.Width);
            int randomY = GameRoot.Random.Next(0, this.playfield.Height);
            return new Point(randomX, randomY);
        }

        #endregion

        #region EventHandlers
        protected void SpacecraftShotHandler(object sender, EventArgs e)
        {
            Spacecraft spacecraft = sender as Spacecraft;

            if (spacecraft != null)
            {
                EntityController.Projectiles.Add(new Projectile(Assets.Visual.BulletModel, spacecraft.Position, spacecraft.Orientation, spacecraft, new Blaster()));
            }

            Assets.Audible.Fire.Play();
        }

        protected void PlayerMoveHandler(object sender, EventArgs e)
        {
            Spacecraft spacecraft = sender as Spacecraft;

            if (spacecraft.Velocity.LengthSquared() > 0.1f)
            {
                this.backgroundGrid.ApplyExplosiveForce(40f, new Vector3(spacecraft.Position2D, 0), 50f);
                // set up some variables
                var particleOrientation = spacecraft.Position2D.ToAngle();
                Quaternion rot = Quaternion.CreateFromYawPitchRoll(0f, 0f, spacecraft.Orientation);

                double t = GameRoot.GameTime.TotalGameTime.TotalSeconds;
                // The primary velocity of the particles is 3 pixels/frame in the direction opposite to which the ship is travelling.
                Vector2 baseVel = new Vector2(spacecraft.Velocity.X, spacecraft.Velocity.Y).ScaleTo(-2);
                // Calculate the sideways velocity for the two side streams. The direction is perpendicular to the ship's velocity and the
                // magnitude varies sinusoidally.
                Vector2 perpVel = new Vector2(baseVel.Y, -baseVel.X) * (1f * (float)Math.Sin(t * 10));
                Vector2 pos = spacecraft.Position2D + Vector2.Transform(new Vector2(-53, 4), rot);   // position of the ship's exhaust pipe.
                const float alpha = 0.7f;

                // middle particle stream
                Vector2 velMid = baseVel + GameRoot.Random.NextVector2(0, 1);
                this.particleManager.CreateParticle(Assets.Visual.Particle, pos, Color.White * alpha, 1f, new Vector2(1, 4),
                    new ParticleState(velMid, ParticleType.Enemy));

                // side particle streams
                Vector2 vel1 = baseVel + perpVel + GameRoot.Random.NextVector2(0, 0.3f);
                Vector2 vel2 = baseVel - perpVel + GameRoot.Random.NextVector2(0, 0.3f);
                this.particleManager.CreateParticle(Assets.Visual.Particle, pos, Color.Orange * alpha, 1f, new Vector2(0.5f, 2),
                    new ParticleState(vel1, ParticleType.Enemy));
                this.particleManager.CreateParticle(Assets.Visual.Particle, pos, Color.Orange * alpha, 1f, new Vector2(0.5f, 2),
                    new ParticleState(vel2, ParticleType.Enemy));
            }
        }

        protected void EnemyDeathHandler(object sender, EventArgs e)
        {
            EnemyShip enemy = sender as EnemyShip;

            for (int i = 0; i < 60; i++)
            {
                float speed = 18f * (1f - 1 / GameRoot.Random.NextFloat(1f, 10f));
                ParticleState particleState = new ParticleState(GameRoot.Random.NextVector2(speed, speed), ParticleType.Enemy, 1f);
                this.particleManager.CreateParticle(Assets.Visual.Particle, enemy.Position2D, enemy.Color, 190, 1.5f, particleState);
            }

            this.backgroundGrid.ApplyExplosiveForce(350f, new Vector3(enemy.Position2D, 1), 80f);

            Assets.Audible.Explosion.Play();
        }

        protected void PlayerDamageTake(object sender, EventArgs e)
        {
            PlayerShip ship = sender as PlayerShip;

            if (ship.Health < 100)
            {
                Assets.Audible.PlayerTakeCriticalDamage.Play();
            }
            else
            {
                Assets.Audible.PlayerTakeCriticalDamage.Play();
            }
        }

        #endregion
    }
}
