﻿namespace OrbitalDefence.Controller.Stages
{
    using OrbitalDefence.Model;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;
    using System.Collections.Generic;
    using OrbitalDefence.Model.Enumerations;

    public class MainMenuStage : Stage
    {
        List<Button> buttons;

        public MainMenuStage(Song backgroundMusic, Camera2D camera, GameStage type)
            : base(backgroundMusic, camera,type)
        { 
        }

        public override void Initialize()
        {
            //this.isExpired = true;
            this.buttons = new List<Button>();

            this.buttons.Add(new Button(Assets.Visual.StartButton, new Rectangle(700, 350, 550, 100)));

            this.buttons.Add(new Button(Assets.Visual.StartButton, new Rectangle(905, 750, 550, 100)));

            this.BackgroundMusic = Assets.Audible.MainMenuMusic;

            base.Initialize();
        }

        public override void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            MouseState mouse = Mouse.GetState();
           
            foreach (Button button in buttons)
            {
                button.Update(mouse);
            }

            if (buttons[0].isClicked)
            {
                this.isExpired = true;
            }

            if (buttons[0].hover)
            {
                buttons[0].Color = Color.Yellow;
            }
            else
            {
                buttons[0].Color = Color.White;
            }

            if (buttons[1].hover)
            {
                buttons[1].Color = Color.Yellow;
            }
            else
            {
                buttons[1].Color = Color.White;
            }

            if (buttons[1].isClicked)
            {
                this.ExitGame = true;
            }
        }

        public override void Draw(SpriteBatch spriteBatch, bool bloomPass)
        {
            spriteBatch.DrawString(Assets.Visual.FontLarge, "orbital defence", new Vector2(1920/3, 150), Color.White);

            spriteBatch.DrawString(Assets.Visual.FontSmall, "start game", new Vector2(880, 400), buttons[0].Color);

            spriteBatch.DrawString(Assets.Visual.FontSmall, "quit game", new Vector2(905,750), buttons[1].Color);
        }
    }
}
