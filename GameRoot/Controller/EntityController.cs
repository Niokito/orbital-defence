﻿namespace OrbitalDefence.Controller
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Microsoft.Xna.Framework;
    using Physics.Library;
    using Physics.WarpingGrid;
    using Physics.ParticleEffects;
    using OrbitalDefence.Model;
    using OrbitalDefence.Model.Enumerations;
    using System.Threading.Tasks;
    using OrbitalDefence.Model.Units;

    public static class EntityController
    {


        public static List<Entity> Entities
        {
            get
            {
                return new List<Entity>(PlayerAssets.Concat<Entity>(EnemyShips).ToList().Concat<Entity>(Projectiles).ToList());
            }
        }

        public static Rectangle Playfield { get; set; }

        public static List<Spacecraft> Ships
        {
            get
            {
                return new List<Spacecraft>(PlayerAssets.Concat<Spacecraft>(EnemyShips));
            }
        }

        public static List<EnemyShip> EnemyShips { get; set; }

        public static List<Spacecraft> PlayerAssets { get; set; }

        public static List<Projectile> Projectiles { get; set; }

        public static List<Tuple<Spacecraft, EnemyShip>> ShipToShipCollisionObjects
        {
            get
            {
                return new List<Tuple<Spacecraft, EnemyShip>>(PlayerAssets.SelectMany(ps => EnemyShips.Select(es => Tuple.Create(ps, es))));
            }
        }

        static EntityController()
        {
            EntityController.EnemyShips = new List<EnemyShip>();
            EntityController.PlayerAssets = new List<Spacecraft>();
            EntityController.Projectiles = new List<Projectile>();
        }

        public static void Update(GameTime gameTime)
        {
            EntityController.UpdatePlayers(gameTime);
            EntityController.UpdateEnemies(gameTime);
            EntityController.UpdateProjectiles(gameTime);
            EntityController.CheckForCollisions();
        }

        public static void UpdatePlayers(GameTime gameTime)
        {
            for (int i = 0; i < EntityController.PlayerAssets.Count; i++)
            {
                EntityController.PlayerAssets[i].Update();
            }
        }

        public static void UpdateEnemies(GameTime gameTime)
        {
            for (int i = 0; i < EntityController.EnemyShips.Count; i++)
            {
                if (EntityController.EnemyShips[i].IsExpired)
                {
                    EntityController.EnemyShips.RemoveAt(i);
                    continue;
                }



                foreach (Spacecraft spacecraft in EntityController.PlayerAssets)
                {
                    if (!EntityController.EnemyShips[i].TargetLock)
                    {
                        var closestPlayerAsset = PlayerAssets.OrderBy(p => Vector2.Distance(p.Position2D, EntityController.EnemyShips[i].Position2D)).First();
                        EntityController.EnemyShips[i].Target = closestPlayerAsset;
                    }
                 
                    if (GameRoot.Random.Next(0, 1000) == 1)
                    {
                        EntityController.EnemyShips[i].TargetLock = true;
                    }

                    if (EntityController.EnemyShips[i].TargetLock && GameRoot.Random.Next(0, 1000) < 5 && EntityController.EnemyShips[i].Target is SpaceStation)
                    {
                        var furthestPlayerAsset = PlayerAssets.OrderBy(p => Vector2.Distance(p.Position2D, EntityController.EnemyShips[i].Position2D)).Last();
                        EntityController.EnemyShips[i].Target = furthestPlayerAsset;
                    }
                    
                    EntityController.EnemyShips[i].Update();

                }

            }
        }

        public static void UpdateProjectiles(GameTime gameTime)
        {
            for (int i = 0; i < EntityController.Projectiles.Count; i++)
            {
                if (Projectiles[i].IsExpired)
                {
                    Projectiles.RemoveAt(i);
                    continue;
                }

                Projectiles[i].Update();

                if (!EntityController.Playfield.Contains(Projectiles[i].Position2D))
                {
                    EntityController.Projectiles[i].IsExpired = true;
                }
            }
        }

        public static void CheckForCollisions()
        {

            if (PlayerAssets[0].BoundingBox.Intersects(PlayerAssets[1].BoundingBox))
            {
                var ship = PlayerAssets[1] as PlayerShip;
                var station = PlayerAssets[0] as SpaceStation;
                ship.Collision(station);
            }

            // Projectile vs SpaceCraft collisions
            try
            {

                Parallel.For(0, EntityController.Projectiles.Count,
                    i =>
                    {
                        for (int j = 0; j < EntityController.Ships.Count; j++)
                        {
                            if (!EntityController.Projectiles[i].IsExpired && EntityController.Ships[j].BoundingBox.Contains(EntityController.Projectiles[i].Position2D) && !EntityController.Projectiles[i].Shooter.Equals(EntityController.Ships[j]))
                            {
                                EntityController.Ships[j].Collision(EntityController.Projectiles[i]);
                            }
                        }
                    }
                );
            } catch (AggregateException e)
            {
                // Dirty last minute fix for weird thread exception. Lists are not thread safe
            }

            // PlayerShip v EnemyShip collisions
            try
            {
                Parallel.For(0, EntityController.ShipToShipCollisionObjects.Count,
                    i =>
                    {
                        if (EntityController.ShipToShipCollisionObjects[i].Item1.BoundingBox.Intersects(EntityController.ShipToShipCollisionObjects[i].Item2.BoundingBox))
                        {
                            EntityController.ShipToShipCollisionObjects[i].Item1.Collision(EntityController.ShipToShipCollisionObjects[i].Item2);
                            EntityController.ShipToShipCollisionObjects[i].Item2.Collision(EntityController.ShipToShipCollisionObjects[i].Item1);

                        }
                    }
                );
            }
            catch (AggregateException e)
            {
                // Dirty last minute fix for weird thread exception. Lists are not thread safe
            }
        }

        public static void KillAll()
        {
            foreach (EnemyShip enemy in EnemyShips)
            {
                enemy.IsExpired = true;
            }
        }
    }
}
