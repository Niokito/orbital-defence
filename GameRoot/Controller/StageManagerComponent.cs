﻿namespace OrbitalDefence.Controller
{
    using OrbitalDefence.Model;
    using OrbitalDefence.Controller.Stages;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;
    using System.Collections.Generic;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Media;
    using Microsoft.Xna.Framework.Audio;
    using System;
    using OrbitalDefence.Model.Enumerations;

    public class StageManagerComponent : GameComponent
    {        
        public StageManagerComponent(GameRoot game)
            : base(game)
        {
            this.Game = game;
            this.Camera = new Camera2D();
        }

        public Camera2D Camera { get; private set; }

        public new GameRoot Game { get; private set; }

        public Stage CurrentStage { get; private set; }

        public static Player Player { get; private set; }

        public static void AddPlayer()
        {
            StageManagerComponent.Player = new Player(0);
        }

        public override void Initialize()
        {
            this.LoadAssets();
            this.CurrentStage = new IntroStage(Assets.Audible.MainMenuMusic, this.Camera, GameStage.Intro);
            this.CurrentStage.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();

            if (CurrentStage.isExpired)
            {
                SwitchStage(CurrentStage.Type);
                this.CurrentStage.Initialize();
            }

            if (CurrentStage.ExitGame)
            {
                this.Game.Exit();
            }

            CurrentStage.Update(gameTime, keyboardState);

            base.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch, bool bloomPass)
        {
            this.CurrentStage.Draw(spriteBatch, bloomPass);
        }

        private void SwitchStage(GameStage Type)
        {
            switch (Type)
            {
                case GameStage.Intro:
                    this.CurrentStage = new MainMenuStage(Assets.Audible.MainMenuMusic, this.Camera, GameStage.MainMenu);
                    break;
                case GameStage.MainMenu:
                    this.CurrentStage = new GameplayStage(this.Game.ScreenWidth, this.Game.ScreenHeight, Camera, Assets.Audible.InGameMusic, GameStage.Gameplay);
                    break;
                case GameStage.Gameplay:
                    this.CurrentStage = new GameOverStage(Assets.Audible.MainMenuMusic, this.Camera, GameStage.GameOver);
                    break;
                case GameStage.GameOver:
                    this.CurrentStage = new MainMenuStage(Assets.Audible.MainMenuMusic, this.Camera, GameStage.MainMenu);
                    break;
                default:
                    break;
            }
        }
        private void LoadAssets()
        {
            Assets.Visual.FontSmall = this.Game.Content.Load<SpriteFont>("Art/Font12");
            Assets.Visual.FontLarge = this.Game.Content.Load<SpriteFont>("Art/Font24");

            Assets.Visual.Enemy1 = this.Game.Content.Load<Texture2D>("Art/enemy1");
            Assets.Visual.Enemy2 = this.Game.Content.Load<Texture2D>("Art/enemy2");
            Assets.Visual.Enemy3 = this.Game.Content.Load<Texture2D>("Art/enemy3");
            Assets.Visual.Enemy4 = this.Game.Content.Load<Texture2D>("Art/enemy4");
            Assets.Visual.Enemy5 = this.Game.Content.Load<Texture2D>("Art/enemy5");
            Assets.Visual.Enemy6 = this.Game.Content.Load<Texture2D>("Art/enemy6");

            Assets.Visual.SpaceStationLevel1 = this.Game.Content.Load<Texture2D>("Art/spaceStationLevel1");
            Assets.Visual.Bars = this.Game.Content.Load<Texture2D>("Art/Health");

            Assets.Visual.GameOver = this.Game.Content.Load<Texture2D>("Art/GameOver");

            Assets.Visual.Particle = this.Game.Content.Load<Texture2D>("Art/particle");

            Assets.Visual.PlayerShipModel = this.Game.Content.Load<Texture2D>("Art/playership");
            Assets.Visual.BulletModel = this.Game.Content.Load<Texture2D>("Art/bullet");
            //Assets.Visual.Bars = this.Game.Content.Load<Texture2D>("Art/Health");

            Assets.Visual.BackgroundGridTexture = new Texture2D(this.Game.GraphicsDevice, 1, 1);
            Assets.Visual.BackgroundGridTexture.SetData(new[] { Color.White });

            // Load music here
            Assets.Audible.InGameMusic = this.Game.Content.Load<Song>("Sounds/reaper");
            Assets.Audible.MainMenuMusic = this.Game.Content.Load<Song>("Sounds/menu");

            // Load sound effects here
            Assets.Audible.Fire = this.Game.Content.Load<SoundEffect>("Sounds/SEffects/explosionSmall");
            Assets.Audible.Explosion = this.Game.Content.Load<SoundEffect>("Sounds/SEffects/explosionBig2");
            Assets.Audible.PlayerTakeDamage = this.Game.Content.Load<SoundEffect>("Sounds/SEffects/player_pain");
            Assets.Audible.PlayerTakeCriticalDamage = this.Game.Content.Load<SoundEffect>("Sounds/SEffects/player_critical");

            // Load fonts here
            Assets.Visual.FontSmall = this.Game.Content.Load<SpriteFont>("Art/Font12");
            Assets.Visual.FontLarge = this.Game.Content.Load<SpriteFont>("Art/Font24");
        }


    }
}
