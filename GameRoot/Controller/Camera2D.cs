﻿namespace OrbitalDefence
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using System;

    public class Camera2D
    {
        public Viewport View { get; set; }
        
        public Matrix? Transformation { get; private set; }

        public void Update(Vector2 position)
        {
            this.Transformation = Matrix.CreateScale(new Vector3(1, 1, 1)) * Matrix.CreateTranslation(new Vector3(-position.X + (this.View.Width / 2), -position.Y + (this.View.Height / 2), 0));
        }
    }
}