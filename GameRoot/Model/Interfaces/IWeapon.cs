﻿namespace OrbitalDefence.Model.Interfaces
{
    public interface IWeapon
    {
        int Damage { get; }

        int FireRate { get; }

        int FireRange { get; }
    }
}
