﻿namespace Physics.Library
{
    using Physics.WarpingGrid;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using System;

    public static class Extensions
    {
        public static float ToAngle(this Vector2 vector)
        {
            return (float)Math.Atan2(vector.Y, vector.X);
        }

        public static void DrawLine(this SpriteBatch spriteBatch, Texture2D texture, Vector2 start, Vector2 end, Color color, float thickness)
        {
            Vector2 delta = end - start;
            spriteBatch.Draw(texture, start, null, color, delta.ToAngle(), new Vector2(0, 0.5f), new Vector2(delta.Length(), thickness), SpriteEffects.None, 0f);
        }

        public static Vector2 ToVec2(this Vector3 vector, Viewport viewport)
        {
            // do a perspective projection
            float factor = (vector.Z + 2000) / 2000;
            return ((new Vector2(vector.X, vector.Y) - (new Vector2(viewport.Width, viewport.Height) / 2f)) * factor) + (new Vector2(viewport.Width, viewport.Height) / 2);
        }

        public static float NextFloat(this Random rand, float minValue, float maxValue)
        {
            return (float)rand.NextDouble() * (maxValue - minValue) + minValue;
        }

        public static Vector2 NextVector2(this Random rand, float minLength, float maxLength)
        {
            double theta = rand.NextDouble() * 2 * Math.PI;
            float length = rand.NextFloat(minLength, maxLength);
            return new Vector2(length * (float)Math.Cos(theta), length * (float)Math.Sin(theta));
        }

        public static Vector2 NextVector2(this Random rand, float xMin, float xMax, float yMin, float yMax)
        {
            return new Vector2(rand.NextFloat(xMin, xMax), rand.NextFloat(yMin, yMax));
        }

        public static Vector2 ScaleTo(this Vector2 vector, float length)
        {
            return vector * (length / vector.Length());
        }
    }
}
