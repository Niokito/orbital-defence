﻿namespace Physics.Library
{
    using Physics.Library;
    using Microsoft.Xna.Framework;

    public abstract class RigidBody3D
    {
        public Vector3 Position { get; set; }

        public Vector3 Velocity { get; set; }

        public float InverseMass { get; private set; }

        protected Vector3 acceleration;

        protected float drag;

        public RigidBody3D(Vector3 position, float inverseMass, float drag)
        {
            this.Position = position;
            this.InverseMass = inverseMass;
            this.drag = drag;
        }

        public virtual void ApplyForce(Vector3 force)
        {
            this.acceleration += force * InverseMass;
        }

        public virtual void Update()
        {
            this.Velocity += acceleration;
            this.Position += Velocity;

            this.acceleration = Vector3.Zero;

            if (this.Velocity.LengthSquared() < 0.001f * 0.001f)
            {
                this.Velocity = Vector3.Zero;
            }

            this.Velocity *= this.drag;
        }

        public abstract void IncreaseDrag(float value);
    }
}
