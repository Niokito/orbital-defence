﻿namespace Physics.WarpingGrid
{
    using Physics.Library;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using System.Collections.Generic;
    using System;
    
    public class Grid
    {
        const float SPRING_STIFFNESS = 0.06f;
        const float SPRING_DAMPING = 0.06f;

        protected static GraphicsDevice graphicsDevice;

        protected Spring[] springs;
        protected PointMass[,] intersectionPoints;

        protected static Texture2D springTexture;
        protected static Color springColor;

        public Grid(Rectangle size, Vector2 spacing, Texture2D springTexture, Color color)
        {
            Grid.springTexture = springTexture;
            Grid.springColor = color;

            List<Spring> springList = new List<Spring>();

            int numColumns = (int)(size.Width / spacing.X) + 1;
            int numRows = (int)(size.Height / spacing.Y) + 1;

            this.intersectionPoints = new PointMass[numColumns, numRows];

            PointMass[,] fixedPoints = new PointMass[numColumns, numRows];

            int column = 0;
            int row = 0;

            for (float y = size.Top; y <= size.Bottom; y += spacing.Y)
            {
                for (float x = size.Left; x <= size.Right; x += spacing.X)
                {
                    intersectionPoints[column, row] = new PointMass(new Vector3(x, y, 0), 1);
                    fixedPoints[column, row] = new PointMass(new Vector3(x, y, 0), 0);
                    column++;
                }

                row++;
                column = 0;
            }

            // link the point masses with springs
            for (int y = 0; y < numRows; y++)
            {
                for (int x = 0; x < numColumns; x++)
                {
                    if (x == 0 || y == 0 || x == numColumns - 1 || y == numRows - 1) // anchor the border of the grid 
                    {
                        springList.Add(new Spring(fixedPoints[x, y], intersectionPoints[x, y], 0.1f, 0.1f));
                    }
                    else if (x % 3 == 0 && y % 3 == 0) // loosely anchor 1/9th of the point masses 
                    {
                        springList.Add(new Spring(fixedPoints[x, y], intersectionPoints[x, y], 0.002f, 0.02f));
                    }

                    if (x > 0)
                    {
                        springList.Add(new Spring(intersectionPoints[x - 1, y], intersectionPoints[x, y], SPRING_STIFFNESS, SPRING_DAMPING));
                    }

                    if (y > 0)
                    {
                        springList.Add(new Spring(intersectionPoints[x, y - 1], intersectionPoints[x, y], SPRING_STIFFNESS, SPRING_DAMPING));
                    }
                }
            }

            springs = springList.ToArray();
        }

        public void Update()
        {
            foreach (Spring spring in this.springs)
            {
                spring.Update();
            }

            foreach (PointMass point in this.intersectionPoints)
            {
                point.Update();
            }
        }

        public void Draw(SpriteBatch spriteBatch, Viewport viewport, Vector2 centerPosition)
        {
            int width = intersectionPoints.GetLength(0);
            int height = intersectionPoints.GetLength(1);

            Vector2 left = new Vector2();
            Vector2 up = new Vector2();
            Vector2 point = new Vector2();
            Vector2 upLeft = new Vector2();

            Rectangle bounds;

            bounds = new Rectangle(
                (int)(centerPosition.X - (viewport.Width / 2)) - 50,
                (int)(centerPosition.Y - (viewport.Height / 2)) - 50,
                viewport.Width + viewport.Width + 50,
                viewport.Height + viewport.Height + 50
            );

            int thickness;

            for (int y = 1; y < height; y++)
            {
                for (int x = 1; x < width; x++)
                {
                    point = intersectionPoints[x, y].Position.ToVec2(viewport);

                    if (x > 1)
                    {
                        left = intersectionPoints[x - 1, y].Position.ToVec2(viewport);

                        if (!bounds.Contains(left.ToPoint()))
                        {
                            continue;
                        }

                        thickness = y % 3 == 1 ? 2 : 1;

                        // use Catmull-Rom interpolation to help smooth bends in the grid
                        int clampedX = Math.Min(x + 1, width - 1);
                        Vector2 mid = Vector2.CatmullRom(intersectionPoints[x - 2, y].Position.ToVec2(viewport), left, point, intersectionPoints[clampedX, y].Position.ToVec2(viewport), 0.5f);

                        // If the grid is very straight here, draw a single straight line. Otherwise, draw lines to our
                        // new interpolated midpoint
                        if (Vector2.DistanceSquared(mid, (left + point) / 2) > 1)
                        {
                            spriteBatch.DrawLine(Grid.springTexture, left, mid, Grid.springColor, thickness);
                            spriteBatch.DrawLine(Grid.springTexture, mid, point, springColor, thickness);
                        }
                        else
                        {
                            spriteBatch.DrawLine(Grid.springTexture, left, point, Grid.springColor, thickness);
                        }
                    }

                    if (y > 1)
                    {
                        up = intersectionPoints[x, y - 1].Position.ToVec2(viewport);
                        thickness = x % 3 == 1 ? 2 : 1;

                        spriteBatch.DrawLine(Grid.springTexture, up, point, Grid.springColor, thickness);
                    }

                    // Spring interpolation
                    if (x > 1 && y > 1)
                    {
                        upLeft = intersectionPoints[x - 1, y - 1].Position.ToVec2(viewport);
                        spriteBatch.DrawLine(Grid.springTexture, 0.5f * (upLeft + up), 0.5f * (left + point), Grid.springColor, 1f); // vertical
                        spriteBatch.DrawLine(Grid.springTexture, 0.5f * (upLeft + left), 0.5f * (up + point), Grid.springColor, 1f); // horizontal
                    }
                }
            }
        }

        public void ApplyDirectedForce(Vector3 force, Vector3 position, float radius)
        {
            foreach (var mass in intersectionPoints)
            {
                if (Vector3.DistanceSquared(position, mass.Position) < radius * radius)
                {
                    mass.ApplyForce(10 * force / (10 + Vector3.Distance(position, mass.Position)));
                }
            }
        }

        public void ApplyImplosiveForce(float force, Vector3 position, float radius)
        {
            foreach (var mass in intersectionPoints)
            {
                float dist2 = Vector3.DistanceSquared(position, mass.Position);
                if (dist2 < radius * radius)
                {
                    mass.ApplyForce(10 * force * (position - mass.Position) / (100 + dist2));
                    mass.IncreaseDrag(0.6f);
                }
            }
        }

        public void ApplyExplosiveForce(float force, Vector3 position, float radius)
        {
            foreach (var mass in intersectionPoints)
            {
                float dist2 = Vector3.DistanceSquared(position, mass.Position);

                if (dist2 < radius * radius)
                {
                    mass.ApplyForce(100 * force * (mass.Position - position) / (10000 + dist2));
                    mass.IncreaseDrag(0.6f);
                }
            }
        }
    }
}
