﻿namespace OrbitalDefence.Model
{
    using Microsoft.Xna.Framework.Input;
    using OrbitalDefence.Model.Enumerations;    
    using System.Collections.Generic;
    using System;
    
    public class Player
    {
        public delegate void KeyHandler();

        public static List<Dictionary<Keys, Command>> ControlSchemes = new List<Dictionary<Keys, Command>>
        {
            new Dictionary<Keys, Command>
            {
                { Keys.Up,      Command.FireEngines },
                { Keys.Left,    Command.TurnLeft },
                { Keys.Right,   Command.TurnRight },
                { Keys.Space,   Command.Fire }
            }
        };

        protected int index;

        public Player(int index)
        {
            this.index = index;
            this.ControlScheme = Player.ControlSchemes[index];
            this.ControlMap = new Dictionary<Command, KeyHandler>();
        }

        public PlayerShip ControlledUnit { get; private set; }

        public Dictionary<Keys, Command> ControlScheme { get; private set; }

        public Dictionary<Command, KeyHandler> ControlMap { get; private set; }
     
        public bool isAlive
        {
            get
            {
                return !(this.ControlledUnit == null);
            }
        }

        public void SetNewControlledUnit(PlayerShip unit)
        {
            this.ControlledUnit = unit;

            this.ControlMap[Command.Fire] = new KeyHandler(this.ControlledUnit.Shoot);
            this.ControlMap[Command.TurnLeft] = new KeyHandler(this.ControlledUnit.TurnLeft);
            this.ControlMap[Command.TurnRight] = new KeyHandler(this.ControlledUnit.TurnRight);
            this.ControlMap[Command.FireEngines] = new KeyHandler(this.ControlledUnit.MoveForward);
        }
    }
}
