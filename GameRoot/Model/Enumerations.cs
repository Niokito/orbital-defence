﻿namespace OrbitalDefence.Model.Enumerations
{
    public enum Command
    {
        TurnLeft, TurnRight, FireEngines,
        Fire
    }
    public enum EnemyType
    {
        Drone, Fighter, StealthDrone
    }
    public enum GameStage
    {
        Intro, MainMenu, Gameplay, GameOver
    }
}
