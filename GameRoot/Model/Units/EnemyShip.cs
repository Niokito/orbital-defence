﻿namespace OrbitalDefence.Model
{
    using System;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using OrbitalDefence.Model.Enumerations;
    using OrbitalDefence.Model.Interfaces;
    using OrbitalDefence.Model.Units;

    public class EnemyShip : Spacecraft
    {
        private int fireRange = 500;

        private float fireRate;

        private double timeSinceLastShot;


        public DateTime nextBullet;
        public TimeSpan bulletDelay = new TimeSpan(0, 0, 0, 0, 3000);

        protected EnemyShip(Texture2D model, Vector3 position, float inverseMass, float drag, int health, int fireRange, Color color)
            : base(model, position, inverseMass, drag)
        {
            this.Health = health;
            this.Color = color;
            this.Scale = 1f;
            this.weapon = new Blaster();
        }

        public bool IsInFireRangeOfTarget
        {
            get
            {
                return Vector2.Distance(this.Position2D, this.Target.Position2D) < this.fireRange;
            }
        }

        public IWeapon weapon { get; private set; }
        
        public bool TargetLock { get; set; }
        
        public Spacecraft Target { get; set; }

        public override void Shoot()
        {
            if (nextBullet <= DateTime.Now)
            {
                nextBullet = DateTime.Now + bulletDelay;
                base.Shoot();
            }
        }

        public override void Update()
        {
            this.Orientation = (float)Math.Atan2((this.Target.Position2D.Y - this.Position2D.Y), (this.Target.Position2D.X - this.Position2D.X));

            if (Vector2.Distance(this.Position2D, this.Target.Position2D) > this.fireRange)
            {
                this.MoveForward();
            }
            else
            {
                this.Shoot();
            }

            base.Update();
        }

        public void SetOrientation(Vector2 position)
        {
            float distanceX = position.X - this.Position.X;
            float distanceY = position.Y - this.Position.Y;

            this.Orientation = (float)Math.Atan2(distanceY, distanceX);
        }

        public static EnemyShip GetEnemyShip(EnemyType type, Vector3 position)
        {
            if (type == EnemyType.Drone)
            {
                EnemyShip Drone = new EnemyShip(Assets.Visual.Enemy1, position, 1, 0.95f, 10, 0, Color.Red);
                return Drone;
            }

            else if (type == EnemyType.Fighter)
            {
                EnemyShip Fighter = new EnemyShip(Assets.Visual.Enemy4, position, 1.5f, 0.95f, 10, 0, Color.Orange);
                return Fighter;
            }

            else if (type == EnemyType.StealthDrone)
            {
                EnemyShip StealthDrone = new EnemyShip(Assets.Visual.Enemy3, position, 1.5f, 0.95f, 10, 0, Color.Yellow);
                return StealthDrone;
            }

            return new EnemyShip(Assets.Visual.Enemy1, position, 1, 2, 10, 0, Color.Orange);

        }

        internal override void Collision(Spacecraft projectile)
        {
            this.Die();
            base.Collision(projectile);
        }

    }
}
