﻿namespace OrbitalDefence.Model
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using OrbitalDefence.Model.Interfaces;
    using System;

    public class Projectile : Entity
    {
        const float PROJECTILE_FORCE = 20f;
        const float PROJECTILE_MASS = 2f;
        const float PROJECTILE_DRAG = 1.1f;

        public Projectile(Texture2D model, Vector3 position, float orientation, Spacecraft shooter, IWeapon weapon)
            : base(model, position, PROJECTILE_MASS, PROJECTILE_DRAG)
        {
            this.Model = model;
            this.ForceMultiplier = PROJECTILE_FORCE;
            this.Orientation = orientation;
            this.Shooter = shooter;
            this.sourceWeapon = weapon;
        }

        public IWeapon sourceWeapon { get; private set; }
        public Spacecraft Shooter { get; private set; }

        public override void Update()
        {
            var direction = new Vector3((float)Math.Cos(this.Orientation), (float)Math.Sin(this.Orientation), 1);
            this.Position += direction * this.ForceMultiplier;
        }
    }
}