﻿namespace OrbitalDefence.Model.Units
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public class SpaceStation : Spacecraft
    {
        const float INVERSE_MASS = 0.3f;
        const float DRAG = 0.9f;

        public SpaceStation(Texture2D model, Vector3 position)
            : base(model, position, INVERSE_MASS, DRAG)
        {
            this.ForceMultiplier = 0.1f;
        }

        public override void Update()
        {
            this.Orientation += 0.0033f;
            this.MoveForward();
            base.Update();
        }
    }
}
