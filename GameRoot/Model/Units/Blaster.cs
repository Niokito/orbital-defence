﻿namespace OrbitalDefence.Model.Units
{
    using OrbitalDefence.Model.Interfaces;

    public class Blaster : IWeapon
    {
        private int damage;
        
        
        public Blaster()
        {
            this.damage = 20;
        }

        public int Damage
        {
            get
            {
                return this.damage;
            }
        }

        public int FireRate { get; set; }

        public int FireRange { get; set; }
    }
}
