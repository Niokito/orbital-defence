﻿namespace OrbitalDefence.Model
{
    using System;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    public class Spacecraft : Entity
    {
        public delegate void EventHandler(object sender, EventArgs e);

        public event EventHandler OnShoot;
        public event EventHandler OnDeath;
        public event EventHandler OnDamageTake;

        public Spacecraft(Texture2D model, Vector3 position, float inverseMass, float drag)
            : base(model, position, inverseMass, drag)
        {
            this.ForceMultiplier = 0.2f;
            this.Health = 100;
        }

        public float Health { get; protected set; }

        public float TurnSpeed { get; protected set; }
        
        public virtual void ModifyDirection(float value)
        {
            this.Orientation += value;
        }

        public virtual void Die()
        {
            this.Health = 0;
            this.IsExpired = true;

            if (this.OnDeath != null)
            {
                this.OnDeath.Invoke(this, EventArgs.Empty);
            }
        }

        public virtual void Shoot()
        {
            if (this.OnShoot != null)
            {
                this.OnShoot.Invoke(this, EventArgs.Empty);
            }
        }

        internal virtual void TakeDamage(float damage)
        {
            if (this.Health - damage <= 0)
            {
                this.Die();
            }
            else
            {
                this.Health -= damage;
                
                if (this.OnDamageTake != null)
                {
                    this.OnDamageTake.Invoke(this, EventArgs.Empty);
                }
            }
        }

        internal virtual void Collision(Projectile projectile)
        {
            if (projectile.Shooter.GetType().Name != this.GetType().Name)
            {
                projectile.IsExpired = true;
                this.TakeDamage(projectile.sourceWeapon.Damage);
            }
        }

        internal virtual void Collision(Spacecraft projectile)
        {

        }
    }
}
