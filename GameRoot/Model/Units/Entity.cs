﻿namespace OrbitalDefence.Model
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Physics.Library;
    using System;

    public abstract class Entity : RigidBody3D
    {
        public Entity(Texture2D model, Vector3 position, float inverseMass, float drag)
            : base(position, inverseMass, drag)
        {
            this.Model = model;
            this.Orientation = 0f;
            this.Color = Color.White;

            this.Scale = 1f;
        }

        public Color Color { get; protected set; }
        
        public Texture2D Model { get; protected set; }

        public float Orientation { get; set; }

        public float Scale { get; protected set; }

        public bool IsExpired { get; set; }

        public float ForceMultiplier { get; set; }

        public Vector2 Origin
        {
            get
            {
                return new Vector2(this.Model.Width / 2, this.Model.Height / 2);
            }
        }
        
        public Rectangle BoundingBox
        { 
            get
            {
                return new Rectangle(((int)this.Position2D.X - this.Model.Width/2), ((int)this.Position2D.Y - this.Model.Height/2), this.Model.Width, this.Model.Height);
            }
        }

        public Vector2 Position2D
        {
            get
            {
                return new Vector2(this.Position.X, this.Position.Y);
            }
        }

        public override void Update()
        {
            base.Update();
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.Model, this.Position2D, null, this.Color, this.Orientation, this.Origin, 1f, SpriteEffects.None, 1f);
        }

        public override void IncreaseDrag(float amount)
        {
            this.drag += amount;
        }

        public virtual void MoveForward()
        {
            Vector3 force = new Vector3((float)Math.Cos(this.Orientation), (float)Math.Sin(this.Orientation), 1);

            this.ApplyForce(this.ForceMultiplier * force);
        }
    }
}
