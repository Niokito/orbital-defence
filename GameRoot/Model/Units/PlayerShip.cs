﻿namespace OrbitalDefence.Model
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using System.Collections.Generic;
    using Physics.Library;
    using Physics.ParticleEffects;
    using System;
    using OrbitalDefence.Model.Interfaces;
    using OrbitalDefence.Model.Units;

    public class PlayerShip : Spacecraft
    {
        public event EventHandler OnMove;

        public DateTime nextBullet;
        public TimeSpan bulletDelay = new TimeSpan(0, 0, 0, 0, 350);
        public Rectangle rectangle;

        public Rectangle healthBarRectangle;
        public Rectangle fuelBarRectangle;

        public List<Projectile> Projectiles { get; private set; }
        public PlayerShip(Texture2D model, Vector3 position, float inverseMass, float drag)
            : base(model, position, inverseMass, drag)
        {
            this.Health = 500;
            this.Fuel = 500;
            this.Projectiles = new List<Projectile>();
            this.Scale = 1f;
            this.TurnSpeed = 0.05f;
            this.Weapon = new Blaster();
        }

        public IWeapon Weapon { get; private set; }

        public float Fuel { get; set; }

        public override void Update()
        {
            fuelBarRectangle = new Rectangle((int)Position.X - 900, (int)Position.Y - 450, (int)this.Fuel, 20);
            healthBarRectangle = new Rectangle((int)Position.X - 900, (int)Position.Y - 500,  (int)this.Health, 20);
            rectangle = new Rectangle((int)Position.X, (int)Position.Y, Model.Width, Model.Height);
            base.Update();
        }

        public void TurnLeft()
        {
            this.ModifyDirection(-this.TurnSpeed);
        }

        public void TurnRight()
        {
            this.ModifyDirection(this.TurnSpeed);
        }

        public override void Shoot()
        {
            if (nextBullet <= DateTime.Now)
            {
                nextBullet = DateTime.Now + bulletDelay;
                base.Shoot();
            }
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Assets.Visual.Bars, this.healthBarRectangle, Color.WhiteSmoke);
            spriteBatch.Draw(Assets.Visual.Bars, this.fuelBarRectangle, Color.Cyan);

            base.Draw(spriteBatch);
        }
        public override void MoveForward()
        {
            if (this.Fuel > 0)
            {
                if (this.OnMove != null)
                {
                    this.OnMove.Invoke(this, EventArgs.Empty);
                }

                this.Fuel -= 0.1f;

                base.MoveForward();
            }
        }

        internal override void Collision(Spacecraft ship)
        {
            this.TakeDamage(ship.Health);
        }

        internal override void Collision(Projectile projectile)
        {
            projectile.IsExpired = true;
            this.TakeDamage(projectile.sourceWeapon.Damage);
        }

        internal void Collision(SpaceStation station)
        {
            if (this.Health < 500)
            {
                this.Health += 0.1f;
            }

            if (this.Fuel < 500)
            {
                this.Fuel += 1;
            }
        }

    }
}
