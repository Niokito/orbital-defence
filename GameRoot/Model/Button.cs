﻿namespace OrbitalDefence.Model
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using System;
    using System.Collections.Generic;

    public class Button
    {
        public Vector2 size;
        public bool isClicked;
        public bool hover;

        private Texture2D texture;
        private Color colour = new Color(255, 255, 255, 255);
        private Rectangle rectangle;

        public event EventHandler OnClick;

        public Button(Texture2D newTexture, Rectangle rectangle)
        {
            this.texture = newTexture;
            this.rectangle = rectangle;
            this.size = new Vector2(100, 50);
        }

        public Color Color
        {
            get
            {
                return this.colour;
            }

            set
            {
                this.colour = value;
            }
        }

        public void Update(MouseState mouse)
        {
            Rectangle mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1);

            if (this.rectangle.Contains(mouseRectangle))
            {
                this.hover = true;

                if (mouse.LeftButton == ButtonState.Pressed)
                {
                    this.isClicked = true;
                }
            }
            else
            {
                hover = false;
            }


            if (this.hover)
            {
                this.colour.A++;
            }

            if (this.OnClick != null && isClicked == true)
            {
                //this.OnClick.Invoke();
            }
        }

        public void setPosition(Rectangle newPosition)
        {
            this.rectangle = newPosition;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.rectangle, this.colour);
        }
    }
}