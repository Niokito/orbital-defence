﻿namespace OrbitalDefence.Model
{
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Graphics;

    public static class Assets
    {
        public struct Visual
        {
            public static Texture2D PlayerShip;
            public static Texture2D PlayerBase;
            public static Texture2D BackgroundGridTexture;

            public static SpriteFont FontSmall;
            public static SpriteFont FontLarge;

            public static Texture2D PlayerShipModel;
            public static Texture2D Particle;

            public static Texture2D Bars;

            public static Texture2D StartButton;
            public static Texture2D Background;
            public static Texture2D GameOver;

            public static Texture2D BulletModel;
            public static Texture2D Enemy1;
            public static Texture2D Enemy2;
            public static Texture2D Enemy3;
            public static Texture2D Enemy4;
            public static Texture2D Enemy5;
            public static Texture2D Enemy6;
            public static Texture2D Bomb;

            public static Texture2D SpaceStationLevel1;
        }

        public struct Audible
        {
            public static Microsoft.Xna.Framework.Media.Song InGameMusic;
            public static Microsoft.Xna.Framework.Media.Song MainMenuMusic;

            public static SoundEffect PlayerTakeDamage;
            public static SoundEffect PlayerTakeCriticalDamage;

            public static SoundEffect Fire;
            public static SoundEffect Explosion;
            public static SoundEffect PlayerSpawn;

        }
    }
}
