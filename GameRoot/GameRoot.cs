﻿namespace OrbitalDefence
{
    using Model;
    using Controller;
    using PostProcessing.Bloom;
    using Physics.Library;
    using Physics.WarpingGrid;
    using Physics.ParticleEffects;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;
    using System;

    public class GameRoot : Game
    {
        private const int SCREEN_WIDTH = 1920;
        private const int SCREEN_HEIGHT = 1080;

        public static readonly Random Random = new Random();

        public GraphicsDeviceManager graphics;

        public SpriteBatch spriteBatch;

        private StageManagerComponent stageManager;

        public GameRoot()
        {
            this.IsFixedTimeStep = false;

            this.Content.RootDirectory = "Content";

            this.graphics = new GraphicsDeviceManager(this);

            this.Bloom = new BloomComponent(this);
            this.stageManager = new StageManagerComponent(this);

            this.ScreenWidth = SCREEN_WIDTH;
            this.ScreenHeight = SCREEN_HEIGHT;

            Components.Add(this.Bloom);
            Components.Add(this.stageManager);
        }

        public static GameTime GameTime { get; private set; }

        public Camera2D Camera { get; private set; }

        public int ScreenWidth { get; private set; }

        public int ScreenHeight { get; private set; }

        public BloomComponent Bloom { get; private set; }

        protected override void Initialize()
        {
            this.graphics.PreferredBackBufferWidth = ScreenWidth;
            this.graphics.PreferredBackBufferHeight = ScreenHeight;

            graphics.IsFullScreen = true;
            this.IsMouseVisible = true;
            graphics.ApplyChanges();

            this.Camera = new Camera2D();

            this.Bloom.Settings = new BloomSettings(0, 3, 2, 1, 1.5f, 1);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(this.graphics.GraphicsDevice);
        }

        protected override void Update(GameTime gameTime)
        {
            GameRoot.GameTime = gameTime;
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            this.Bloom.BeginDraw();
            spriteBatch.Begin(SpriteSortMode.Texture, BlendState.AlphaBlend, null, null, null, null, this.stageManager.Camera.Transformation);
            
            
            //GameRoot.ParticleManager.Draw(spriteBatch);
            this.stageManager.Draw(spriteBatch, true);

            spriteBatch.End();

            base.Draw(gameTime);
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, this.stageManager.Camera.Transformation);

            this.stageManager.Draw(spriteBatch, false);

            spriteBatch.End();
        }
    }
}
